import React, { useEffect, useState } from 'react';
import Database from 'database-api';

import './LeaderBoard.css';

let database = new Database();

const LeaderBoard = (props) => {
  let Data;
  const [data, setData] = useState([]);

  useEffect(() => {
    postLeaderBoardHandler();
  }, []);

  const postLeaderBoardHandler = () => {
    if (props.post) {
      console.log('POSTING');
      database.postScoreData(props.score).then(() => {
        getLeaderBoardHandler();
      });
    } else {
      getLeaderBoardHandler();
    }
  };

  const getLeaderBoardHandler = () => {
    try {
      database.getLeaderBoard().then((data) => {
        let sortedData = data.sort(
          (a, b) => parseInt(a.score) < parseInt(b.score)
        );
        setData(sortedData);
      });
    } catch (err) {
      throw new Error(err);
    }
  };

  if (data) {
    Data = data.map((element, index) => (
      <div className="row" key={index}>
        <li>{index + 1}</li>
        <li>{element.display_name}</li>
        <li> {element.score}</li>
      </div>
    ));
  }

  return (
    <div className="ldbrd__container">
      <h2>LeaderBoard</h2>
      <ul className="ldbrd__scores">{Data}</ul>
    </div>
  );
};

export default LeaderBoard;
